# Leia-me #

Este foi um projeto a titulo de estudo, apenas para me divertir e exercitar !

### O que é ? ###

* Um jogo da velha.
* Versão 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### O que foi usado no desenvolvimento? ###

* Android Studio 1.5.1
* SDK Mínima = API: 10 Android 2.3.3 (Gingerbread)

### Algumas telas do aplicativo ###

![01.jpg](https://bitbucket.org/repo/y68z6e/images/2985067627-01.jpg)
![04.jpg](https://bitbucket.org/repo/y68z6e/images/428176288-04.jpg)
![02.jpg](https://bitbucket.org/repo/y68z6e/images/3585108341-02.jpg)
![03.jpg](https://bitbucket.org/repo/y68z6e/images/2376995385-03.jpg)

* Acho que você já entendeu ! :)

### Quer brincar com esse projeto? ###

* Ele é publico, basta você baixar e se divertir, aproveita pra melhorar ele ! :)
* Caso tenha alguma sugestão de melhoria ou queira relatar algum bug, informe na opção de incidências do projeto.
* Caso queira entrar em contato comigo para perguntar ou solicitar algo, me envie um email: [brunomeloesilva@gmail.com](Link URL)