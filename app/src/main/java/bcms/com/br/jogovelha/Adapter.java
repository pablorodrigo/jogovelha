package bcms.com.br.jogovelha;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by brunosilva on 20/12/15.
 */
public class Adapter extends BaseAdapter {

    private Activity contexto;
    private String[][] cores;

    public Adapter(Activity contexto, String[][] cores) {
        this.contexto = contexto;
        this.cores = cores;
    }

    @Override
    public int getCount() {
        return cores.length;
    }

    @Override
    public Object getItem(int position) {
        return cores[position];//nem usa !
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflarLayout = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflarLayout.inflate(R.layout.item_adapter, null);

            Button btnCor1 = (Button) convertView.findViewById(R.id.IdButtonCor1);
            Button btnCor2 = (Button) convertView.findViewById(R.id.IdButtonCor2);
            Button btnCor3 = (Button) convertView.findViewById(R.id.IdButtonCor3);
            Button btnCor4 = (Button) convertView.findViewById(R.id.IdButtonCor4);
            Button btnCor5 = (Button) convertView.findViewById(R.id.IdButtonCor5);
            Button[] btns = new Button[]{btnCor1, btnCor2, btnCor3, btnCor4, btnCor5};
            convertView.setTag(btns);
        }

        final Button[] btns = (Button[]) convertView.getTag();
        for (int i=0; i < 5; i++){
            btns[i].setBackgroundColor(Color.parseColor(cores[position][i]));
            final int iAux = i;
            btns[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra("cor", cores[position][iAux]);
                    contexto.setResult(0, i);
                    contexto.finish();
                }
            });
        }

        return convertView;
    }
}
