package bcms.com.br.jogovelha;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Toast;

/**
 * Created by brunosilva on 18/12/15.
 */
public class MainActivity extends AppCompatActivity {

	Velha velha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		//Iniciaiza as cores
		SharedPreferences preferencia = getSharedPreferences("velha", 0);
		Preferencias.CORFUNDO.setColor(preferencia.getInt("CF", Preferencias.CORFUNDO.getColor()));
		Preferencias.COR_LINHA_VERTICAL_ESQUERDA.setColor(preferencia.getInt("CLVE", Preferencias.COR_LINHA_VERTICAL_ESQUERDA.getColor()));
		Preferencias.COR_LINHA_VERTICAL_DIREITA.setColor(preferencia.getInt("CLVD", Preferencias.COR_LINHA_VERTICAL_DIREITA.getColor()));
		Preferencias.COR_LINHA_HORIZONTAL_SUPERIOR.setColor(preferencia.getInt("CLHS", Preferencias.COR_LINHA_HORIZONTAL_SUPERIOR.getColor()));
		Preferencias.COR_LINHA_HORIZONTAL_INFERIOR.setColor(preferencia.getInt("CLHI", Preferencias.COR_LINHA_HORIZONTAL_INFERIOR.getColor()));
		Preferencias.COR_X.setColor(preferencia.getInt("CX", Preferencias.COR_X.getColor()));
		Preferencias.COR_O.setColor(preferencia.getInt("CO", Preferencias.COR_O.getColor()));
		Preferencias.COR_LINHA_GAMEOVER.setColor(preferencia.getInt("CLGO", Preferencias.COR_LINHA_GAMEOVER.getColor()));

		velha = new Velha(this);
		setContentView(velha);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflador = getMenuInflater();
		inflador.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		switch (id) {
			case R.id.IdItemRestart:
				velha.restart();
				Toast.makeText(this, "O jogo foi reiniciado !", Toast.LENGTH_SHORT).show();
				break;
			case R.id.IdItemPreferencias:
				startActivity(new Intent(this, Preferencias.class));
				break;
		}
		return super.onOptionsItemSelected(item);
	}
}
