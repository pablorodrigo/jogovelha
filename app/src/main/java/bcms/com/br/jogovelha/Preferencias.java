package bcms.com.br.jogovelha;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by brunosilva on 18/12/15.
 */
public class Preferencias extends AppCompatActivity {

    public static final Paint CORFUNDO = new Paint();
    public static final Paint COR_LINHA_VERTICAL_ESQUERDA = new Paint();
    public static final Paint COR_LINHA_VERTICAL_DIREITA = new Paint();
    public static final Paint COR_LINHA_HORIZONTAL_SUPERIOR = new Paint();
    public static final Paint COR_LINHA_HORIZONTAL_INFERIOR = new Paint();
    public static final Paint COR_X = new Paint();
    public static final Paint COR_O = new Paint();
    public static final Paint COR_LINHA_GAMEOVER = new Paint();
    public static final int transparenciaLinhas = 20;

    static {
        defaults();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferencias);

        //E tudo mais do mesmo, so muda o requestCode para cada diferente botao !
        ImageButton imgBtn1 = (ImageButton) findViewById(R.id.IdImageButtonCdF);
        imgBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 1);
            }
        });
        ImageButton imgBtn2 = (ImageButton) findViewById(R.id.IdImageButtonCLVE);
        imgBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 2);
            }
        });
        ImageButton imgBtn3 = (ImageButton) findViewById(R.id.IdImageButtonCLVD);
        imgBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 3);
            }
        });
        ImageButton imgBtn4 = (ImageButton) findViewById(R.id.IdImageButtonCLHS);
        imgBtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 4);
            }
        });
        ImageButton imgBtn5 = (ImageButton) findViewById(R.id.IdImageButtonCLHI);
        imgBtn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 5);
            }
        });
        ImageButton imgBtn6 = (ImageButton) findViewById(R.id.IdImageButtonCX);
        imgBtn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 6);
            }
        });
        ImageButton imgBtn7 = (ImageButton) findViewById(R.id.IdImageButtonCO);
        imgBtn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 7);
            }
        });
        ImageButton imgBtn8 = (ImageButton) findViewById(R.id.IdImageButtonCLFJ);
        imgBtn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), PaletaCores.class), 8);
            }
        });

        Button btnSair = (Button) findViewById(R.id.IdButtonSair);
        btnSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button btnDefault = (Button) findViewById(R.id.IdButtonDefault);
        btnDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaults();
                SharedPreferences preferencia = getSharedPreferences("velha", 0);
                SharedPreferences.Editor editorPref = preferencia.edit();
                editorPref.putInt("CF", CORFUNDO.getColor());
                editorPref.putInt("CLVE", COR_LINHA_VERTICAL_ESQUERDA.getColor());
                editorPref.putInt("CLVD", COR_LINHA_VERTICAL_DIREITA.getColor());
                editorPref.putInt("CLHS", COR_LINHA_HORIZONTAL_SUPERIOR.getColor());
                editorPref.putInt("CLHI", COR_LINHA_HORIZONTAL_INFERIOR.getColor());
                editorPref.putInt("CX", COR_X.getColor());
                editorPref.putInt("CO", COR_O.getColor());
                editorPref.putInt("CLGO", COR_LINHA_GAMEOVER.getColor());
                editorPref.commit();
                onResume();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        SharedPreferences preferencia = getSharedPreferences("velha", 0);
        SharedPreferences.Editor editorPref = preferencia.edit();
        switch (requestCode){
            case 1:
                pintar(R.id.IdTextViewCorFundo, CORFUNDO, 0, data);
                editorPref.putInt("CF", CORFUNDO.getColor());
                break;
            case 2:
                pintar(R.id.IdTextViewCorLVE, COR_LINHA_VERTICAL_ESQUERDA, transparenciaLinhas, data);
                editorPref.putInt("CLVE", COR_LINHA_VERTICAL_ESQUERDA.getColor());
                break;
            case 3:
                pintar(R.id.IdTextViewCorLVD, COR_LINHA_VERTICAL_DIREITA, transparenciaLinhas, data);
                editorPref.putInt("CLVD", COR_LINHA_VERTICAL_DIREITA.getColor());
                break;
            case 4:
                pintar(R.id.IdTextViewCorLHS, COR_LINHA_HORIZONTAL_SUPERIOR, transparenciaLinhas, data);
                editorPref.putInt("CLHS", COR_LINHA_HORIZONTAL_SUPERIOR.getColor());
                break;
            case 5:
                pintar(R.id.IdTextViewCorLHI, COR_LINHA_HORIZONTAL_INFERIOR, transparenciaLinhas, data);
                editorPref.putInt("CLHI", COR_LINHA_HORIZONTAL_INFERIOR.getColor());
                break;
            case 6:
                pintar(R.id.IdTextViewCorX, COR_X, transparenciaLinhas, data);
                editorPref.putInt("CX", COR_X.getColor());
                break;
            case 7:
                pintar(R.id.IdTextViewCorBola, COR_O, 0, data);
                editorPref.putInt("CO", COR_O.getColor());
                break;
            case 8:
                pintar(R.id.IdTextViewCLFJ, COR_LINHA_GAMEOVER, 0, data);
                editorPref.putInt("CLGO", COR_LINHA_GAMEOVER.getColor());
                break;
        }
        editorPref.commit();
    }

    private void pintar(int Id, Paint pincel, int alpha, Intent data){
        TextView label = (TextView) findViewById(Id);
        int cor = Color.parseColor(data.getStringExtra("cor"));
        label.setBackgroundColor(cor);
        pincel.setColor(cor);
        if(alpha!=0)
            pincel.setAlpha(alpha);
    }

    private static void defaults(){
        //CORES DEFAULTS: 255 é o valor maximo da transparencia, quando nao ha transparencia.
        //Preto
        CORFUNDO.setARGB(255, 0, 0, 0);
        //DeepSkyBlue
        COR_LINHA_VERTICAL_ESQUERDA.setARGB(transparenciaLinhas, 0, 191, 255);
        COR_LINHA_VERTICAL_DIREITA.setARGB(transparenciaLinhas, 0, 191, 255);
        COR_LINHA_HORIZONTAL_SUPERIOR.setARGB(transparenciaLinhas, 0, 191, 255);
        COR_LINHA_HORIZONTAL_INFERIOR.setARGB(transparenciaLinhas, 0, 191, 255);
        COR_X.setARGB(transparenciaLinhas, 0, 191, 255);
        COR_O.setARGB(255, 0, 191, 255);//nao pude usar a cor padrao devido a transparencia 20 muito fraca
        COR_LINHA_GAMEOVER.setARGB(255, 0, 191, 255);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Paint aux = CORFUNDO;
        ((TextView) findViewById(R.id.IdTextViewCorFundo)).setBackgroundColor(aux.getColor());
        aux = new Paint(COR_LINHA_VERTICAL_ESQUERDA);
        aux.setAlpha(255);
        ((TextView) findViewById(R.id.IdTextViewCorLVE)).setBackgroundColor(aux.getColor());
        aux = new Paint(COR_LINHA_VERTICAL_DIREITA);
        aux.setAlpha(255);
        ((TextView) findViewById(R.id.IdTextViewCorLVD)).setBackgroundColor(aux.getColor());
        aux = new Paint(COR_LINHA_HORIZONTAL_SUPERIOR);
        aux.setAlpha(255);
        ((TextView) findViewById(R.id.IdTextViewCorLHS)).setBackgroundColor(aux.getColor());
        aux = new Paint(COR_LINHA_HORIZONTAL_INFERIOR);
        aux.setAlpha(255);
        ((TextView) findViewById(R.id.IdTextViewCorLHI)).setBackgroundColor(aux.getColor());
        aux = new Paint(COR_X);
        aux.setAlpha(255);
        ((TextView) findViewById(R.id.IdTextViewCorX)).setBackgroundColor(aux.getColor());
        aux = COR_O;
        ((TextView) findViewById(R.id.IdTextViewCorBola)).setBackgroundColor(aux.getColor());
        aux = COR_LINHA_GAMEOVER;
        ((TextView) findViewById(R.id.IdTextViewCLFJ)).setBackgroundColor(aux.getColor());
    }
}
